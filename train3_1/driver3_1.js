#!clsp

/**
	这里还是演示的官方的piggybank的demo,区别就是这里使用的标准硬币来消费官方的piggybank。
	标准硬币：就是你账号里的硬币,大部情况都是用来转账的。
*/

@debug

/** 加载默认助记词 */
@loadMnemonic

/** 部署智能硬币 官方的chialisp */
const [program,smartCoin] = await DEPLOY_SMART_COIN({
	file:"piggybank.clsp",
	curry:[400,$ph],
	amount:300
})

/** 
	从全节点拿到指定哈希的硬币  返回的是抽象出来的一个币，本质上是一串硬币的集合
	默认先把这一串硬币兑换成一个硬币进行消费，这样做是因为一个硬币方便写入solution
 */
const contri_coin = await WRAP_STANDARD_COIN({
	puzzleHash:$ph,
	amount:101
	//fee:100 这里也可以添加费用，和该硬币消费是一起使用，减小打包时间。
})

/** 新的金额 */
const new_amount = piggy_coin.getAmount()+contri_coin.getAmount()

piggy_coin.setSolution([piggy_coin.getAmount(),new_amount,piggy_coin.getPuzzleHash()])

/** 标准硬币这里要添加一个ASSERT_COIN_ANNOUNCEMENT条件，来应对piggybank里生成的CREATE_COIN_ANNOUNCEMENT 条件 */
contri_coin.setSolution([
		[OPC_CODE.ASSERT_COIN_ANNOUNCEMENT,SHA256(`${piggy_coin.getId()} ${new_amount}`)]
	],@sk1)

@mark($ph,new_amount,'CASH OUT PUZZLEHASH')

/** 同时消费 */
await Rpc.of([piggy_coin,contri_coin]).spent().print()

