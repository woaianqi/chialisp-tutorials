#!clsp
/**
		批量转账驱动
		这里使用的是消费标准货币，代码当然简单很多啦。
*/

@debug

/* 加载默认助记词 */
@loadMnemonic

/* 加载第二组助记词 */
@loadMnemonic(1)

/** 需要支付的订单  */
const payList = [
	[OPC_CODE.CREATE_COIN,$ph_1,100],
	[OPC_CODE.CREATE_COIN,$ph_1,80],
]

/** 计算总额 */
const allAmount = payList.map(v => v[2]).sum()

/** 找到满足金额的硬币 */
const contri_coin = await WRAP_STANDARD_COIN({
	puzzleHash:$ph,
	amount:allAmount
})

/** 消费 */
await contri_coin.setSolution(payList,$sk).doSpent().print()

