#!clsp


@include("./deploy5.js")


@mark(@ph2,@xch(0.0004),'pay_puzzle_hash')
@mark(transaction_coin.getPuzzleHash(),null,'transaction_coin')

const spentInfo = [@ph2,@xch(0.0004),@xch(0.0004)]

transaction_coin.setSolution([
	...transaction_coin.attrs(["id","puzzle_hash","amount"]),
	...spentInfo
])

fuelCoinList.forEach(coin => {

	coin.setSolution([
	...transaction_coin.attrs(["id","amount"]),
	...spentInfo
	]).setSignature(spentInfo,@sk1)

})

//硬币绑定花费
await Rpc.of([transaction_coin,...fuelCoinList]).spent().print()


@summary