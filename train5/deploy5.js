#!clsp

@deploy(2)

const [fuelCoinPuzzle,fuelCoin,fuelCoinList] = await OPERATE_DEPLOY({
	file:"fuel.clsp",
	curry:[@pk1],
	fingerPrint:@fp1,
	amount:@xch(0.0002),
	size:2
	})

const [transaction_puzzle,transaction_coin] = await OPERATE_DEPLOY({
	file:"transaction.clsp",
	curry:[@pk1,fuelCoinPuzzle.getPuzzleHash()],
	fingerPrint:@fp1,
	amount:@xch(0.0001)
	})