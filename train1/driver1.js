#!clsp
/**
	编写一个智能硬币，只有在超过指定时间后才可以消费
	消费时为指定地址创建硬币
*/

@debug

/** 加载助记词 */
@loadMnemonic

/**
	部署智能货币
	@now + @minute(5) 等同于 new Date().getTime()/1000+60*5，说白了就是5分钟后的时间戳（单位秒）
*/
const [program,smartCoin] = await DEPLOY_SMART_COIN({
	file:"train1.clsp",
	curry:[$ph,@now + @minute(5)],
	amount:400
})

/** 设置解决方案 */
smartCoin.setSolution([smartCoin.getAmount()])

/**  尝试立即花费，这里肯定会报错，除非部署的时间超过了5分钟 */
smartCoin.doSpent()

/** 程序挂起6分钟 */
await WAIT(@minute(6))

/** 再次花费，这次肯定没有问题了 */
await smartCoin.doSpent().print()