#!clsp
/**
	chialisp使用的还是官方的原版piggybank demo
	这里主要是讲解下如何驱动官方的demo
*/

@debug

/* 加载默认助记词 */
@loadMnemonic

/* 加载第二组助记词 */
@loadMnemonic(1)


/**
	一次部署两个智能硬币
*/
const [ 

	[piggy_programe,piggy_coin],

	[contri_programe,contri_coin] 

] = await DEPLOY_SMART_COIN_LIST(
	[
		{
			file:"piggybank.clsp",
			/**
				500 TARGET_AMOUNT 目标金额
				$ph_1 CASH_OUT_PUZZLE_HASH 存满后支出的地址
				这里执行完等于已经有了200mojo的存钱罐，当存钱罐存到500的时候自动将全部金额打到$ph_1这个地址。
			*/
			curry:[500,$ph_1],
			amount:200
		},
		{
			file:"contribution.clsp",
			/* $pk PUBKEY 绑定的公钥 */
			curry:[$pk],
			amount:400
		}
	]
)

/**
	对支出地址进行标记
	这里存钱罐自身金额200加上贡献金额400，大于目标金额500
	最终支出地址会+600
*/
@mark($ph_1,null,'cash out puzzlehash')

/** 计算总共金额 */
const new_amount = piggy_coin.getAmount()+contri_coin.getAmount()

/** 存钱罐 设置解决方案 */ 
piggy_coin.setSolution([piggy_coin.getAmount(),new_amount,piggy_coin.getPuzzleHash()])

/** 贡献币 设置解决方案 并使用私钥签名 */ 
contri_coin.setSolution([piggy_coin.getId(),new_amount],$sk)

/**
	两个币绑定在一起消费
	因为贡献币其中一个条件:(list ASSERT_COIN_ANNOUNCEMENT (sha256 coin_id new_amount))
	所有就决定了两个币必须绑定在一个spendBundler中进行消费
*/
await Rpc.of([piggy_coin,contri_coin]).spent().print()

