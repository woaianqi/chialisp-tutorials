#!clsp

@debug

/** 加载默认助记词 */
@loadMnemonic

/** 
	加载了第二组助记词
	会生成一下内置变量,就是原先变量名后面加_1
	$sk_1 钱包第一私钥
	$pk_1 钱包第一公钥
	$ph_1 钱包第一地址
*/
@loadMnemonic(1)

/**
 	部署train2.clsp
	PUBKEY 钱包第一公钥
	DELAY_TIME 60*3 延迟花费的时间（秒）
	CASH_OUT_PUZZLE_HASH 硬币花费的支出地址
	这里不同于train1的地方在于，
*/
const [program,smartCoin] = await DEPLOY_SMART_COIN({
	file:"train2.clsp",
	curry:[$pk,@minute(3),$ph_1],
	amount:300
})

/** 标记：第二组助记词地址 金额300 的硬币变动情况 */
@mark($ph_1,300,'CASH_OUT_PUZZLE_HASH')

/** 
	这里setSolution(params:[Any],<privateKey:String>)
	params:必填，解决方案的参数
	privateKey:可选项，如果传入私钥则会对整个param进行签名
	签名过程：
		param所有元素转成字节然后相加得到B,然后计算B的hash得到C,最后用私钥对C进行签名得到最终的签名signature
		伪代码：signature = AugSchemeMPL.sign(私钥, std_hash(  params.map(v => v.bytes).sum()  ))
		对应chialisp中的(list AGG_SIG_UNSAFE PUBKEY (sha256 my_coin_id my_amount))
 */
smartCoin.setSolution([smartCoin.getId(),smartCoin.getAmount()],$sk).doSpent()

/** 等待4分钟 */
await WAIT(@minute(4))

/** 再次尝试花费 */
await smartCoin.doSpent().print()
