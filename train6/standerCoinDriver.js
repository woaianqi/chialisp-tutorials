#!clsp
/**
		批量转账驱动
		这里使用的是消费标准货币，代码当然简单很多啦。
*/

@debug

/* 加载默认助记词 */
@loadMnemonic

/* 加载第二组助记词 */
@loadMnemonic(1)

/** 需要支付的订单 */
const payList = [
	[51,$ph_1,100],
	[51,$ph_1,80],
]

/** 计算总额 */
const allAmount = payList.map(v => v[2]).sum()

/** 找到满足金额的货币 */
const contri_coin = await WRAP_STANDARD_COIN({
	puzzleHash:$ph,
	amount:allAmount
	//fee:100 这里也可以添加费用，和该硬币消费是一起使用，减小打包时间。
})

/** 消费 */
await contri_coin.setSolution(payList,$sk).doSpent().print()

