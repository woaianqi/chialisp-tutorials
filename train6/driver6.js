#!clsp
/**
	chialisp使用的还是官方的原版piggybank demo
	这里主要是讲解下如何驱动官方的demo
*/

@debug

/* 加载默认助记词 */
@loadMnemonic

/* 加载第二组助记词 */
@loadMnemonic(1)


/**
	一次部署两个智能硬币
*/
const [ 

	[transaction_program,transaction_coin],

	[fuelProgram,fuel_coin] 

] = await DEPLOY_SMART_COIN_LIST(
	[
		{
			file:"transaction.clsp",
			/**
				$pk:设置公钥
				$ph:剩余金额打到的地址
			*/
			curry:[$pk,$ph],
			amount:1
		},
		{
			file:"fuel.clsp",
			/**
				$pk:设置公钥
			*/
			curry:[$pk],
			amount:200
		}
	]
)

/** 设置支付列表 */
const payList = [
		[$ph_1,80],
		[$ph_1,90]
	]

/** 总共支出的金额 */
const paySum = payList.map(v => v[1]).sum()

/** 支出信息 */
const spentInfo = [payList,paySum,fuel_coin.getAmount()]


/** 两个硬币设置各自的解决方案 */
transaction_coin.setSolution([
	...transaction_coin.attrs(["id","puzzle_hash","amount"]),
	...spentInfo
]).setSignature(spentInfo,$sk)

fuel_coin.setSolution([transaction_coin.getId(),fuel_coin.getAmount()],$sk)

//绑定两个硬币同时花费 打印花费结果
await Rpc.of([transaction_coin,fuelCoinList]).spent().print();