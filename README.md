# chialisp-tutorials

#### 介绍
- chialisp的一些教程，包括官方piggybank的demo实现。
- 驱动代码使用我自己写的[chialisp-preclsp](https://gitee.com/woaianqi/chialisp-preclsp)
- 项目初衷：用最简单的代码展示chialisp的魅力，让更多的开发者了解chialisp，贡献chia生态。
- 教程视频等我孩子出生了再做吧，官方的demo很多原理都没讲，有点让初入区块链的新手无从下手（包括😁）。
- 感谢chia团队，让区块链如此有趣。


#### 运行
```shell
#进入课程目录 然后运行驱动代码
cd trainX && preclsp driverX.js
```

#### 课程目录
| 目录       | 		   课程名       | 	介绍     											|  状态         |
| ----------| :-----------:  		| :-----------: 										|:-----------: |
| train0    | 驱动代码演示      		| 演示如何使用preclsp部署和消费一个货币     				| 完成   	   |
| train1    | 锁定货币          		|   硬币锁仓  		  									| 完成   	   |
| train2    | 货币签名         		|   硬币锁仓+签名      									| 完成		   |  完成
| train3    | 官方的piggybank项目    | 驱动官方的piggybank项目 使用两个智能硬币消费     		| 完成   	   |
| train3_1  | 官方的piggybank项目    | 驱动官方的piggybank项目,使用智能硬币+标准硬币消费  		| 完成   	   |
| train4    | 批量转帐          		| 部署一个可以批量转帐的智能货币，模拟发工资的场景  			| 完成 		   |
| train5    | 移动钱包          		| 编写一个网页上使用的去中心化钱包              			| 		       |



#### 联系方式
- QQ:892280082
- EMAIL:892280082@qq.com



