#!clsp
/**
	一个简单的智能硬币演示
	该智能硬币消费时会把余额打给地址TARGET_PUZZLE_HASH

	注意：该智能硬币设计是不安全的，因为我注释了;(list ASSERT_MY_AMOUNT my_amount)这一行
	恶意的节点是会修改你的参数my_amount。
	例如你传入的是300，旷工改成1，那剩下的299就会被旷工白嫖了哦。
*/

/**开启debug模式，当前目录会产生运行时的临时文件和日志，以便调试*/
@debug


/**
	这个注解可以让下面加载的助记词生成的地址为线上地址 例如：xchadf23423sdadf
	否则默认生成的是测试地址:txchxxxx
	@mainnet 
*/

/**
	加载助记词
	递归查询5层父级目录的preclsp.json文件
	读取@mnemonic助记词信息，然后生成以下内置变量
	部署货币均使用该助记词的第一个地址上的币
	$sk（钱包第一私钥）,
	$pk（钱包第一公钥）,
	$ph（钱包第一地址puzzleHash）,
	$address （钱包第一地址encode(puzzleHash)）,
*/
@loadMnemonic

/**
	无参情况下展示$ph的余额
	调用@balance(str:puzzlehash) 可以显示指定的余额
*/
@balance

/**
	调用标准硬币，设置好解决方案[CREATE_COIN,program.puzzleHash,300]
	通过rpc部署train0.clsp到链上。
	重点：这种部署方法不需要同步钱包！
	最后得到结果
	program： train0.clsp（curry化后） 加载到内存的程序
	smartCoin： 部署成功后的智能硬币
*/
const [program,smartCoin] = await DEPLOY_SMART_COIN({
	file:"train0.clsp",
	curry:[$ph],
	amount:300
})

/**
	标记地址和金额，脚本运行结束后可以查看到变动情况
	标记 $ph地址上，金额为300 的硬币变动
	例如脚本结束后显示:

	@mark puzzle_hash:0xa1f222c319199f0ef7aa2db67b883647a9056fbe9c1f13468fec2c789cf0c36a amount:300---
	@SUMMARY:  puzzle_hash:0xa1f222c319199f0ef7aa2db67b883647a9056fbe9c1f13468fec2c789cf0c36a  search_amount:300
	{
	  "title": "TARGET_PUZZLE_HASH",
	  "puzzle_hash": "0xa1f222c319199f0ef7aa2db67b883647a9056fbe9c1f13468fec2c789cf0c36a",
	  "search_amount": 300,
	  "total_size": "+1      (2 -> 3)",
	  "spent_size": "0      (1 -> 1)",
	  "unspent_size": "+1      (1 -> 2)",
	  "spent_amount": "0      (300 -> 300)",
	  "un_spent_amount": "+300      (300 -> 600)",
	  "spent_amount_xch": "0      (0.0000000003 -> 0.0000000003)",
	  "un_spent_amount_xch": "+3e-10      (0.0000000003 -> 0.0000000006)"
	}

*/
@mark($ph,smartCoin.getAmount(),'TARGET_PUZZLE_HASH')

/**
	智能硬币smartCoin设置解决方案[smartCoin.getAmount()]
	实际上就是传入train0.clsp程序curry后剩余的参数my_amount
	然后调用rpc进行消费，并且打印消费后的硬币信息
*/
await smartCoin.setSolution([smartCoin.getAmount()]).doSpent().print()





